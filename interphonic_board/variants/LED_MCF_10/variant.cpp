/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


/*
Modify for Support Interphonic Arduino Project (SAMD21) MUSICAL_FOUNTAIN_BOARD_1.0
*/


/*
 * +==================================================================================================================+
 * | Pin number |  PIN   |  A  |              B               |   C    |    D    |   E    |   F    |   G    |    H    |
 * |            |        |-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |            |        | EIC |  ADC  |  AC  |  PTC  |  DAC  | SERCOM | SERCOM+ | TC/TCC |   TCC  |  COM   | AC/GCLK |
 * |============+========+=====+=======+======+=======+=======+========+=========+========+========+========+=========|
 * |   Digital                                                                                                        |                                                                  
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |     0      | PA[ 0] |EXT00|       |      |       |       |        | SCOM1.0 |TCC2/WO0|        |        |         |
 * |     1      | PA[ 1] |EXT01|       |      |       |       |        | SCOM1.1 |TCC2/WO1|        |        |         |
 * |     2      | PA[ 2] |EXT02| AIN00 |      |  Y00  | Vout  |        |         |        |TCC3/WO0|        |         |
 * |     3      | PA[ 3] |EXT03| AIN01 |      |  Y01  |       |        |         |        |TCC3/WO1|        |         |
 * |     4      | PA[ 4] |EXT04| AIN04 | AIN00|  Y02  |       |        | SCOM0.0 |TCC0/WO0|TCC3/WO2|        |         |
 * |     5      | PA[ 5] |EXT05| AIN05 | AIN01|  Y03  |       |        | SCOM0.1 |TCC0/WO1|TCC3/WO3|        |         |
 * |     6      | PA[ 6] |EXT06| AIN06 | AIN02|  Y04  |       |        | SCOM0.2 |TCC1/WO0|TCC3/WO4|        |         |
 * |     7      | PA[ 7] |EXT07| AIN07 | AIN03|  Y05  |       |        | SCOM0.3 |TCC1/WO1|TCC3/WO5|I2S/SD0 |         |
 * |     8      | PA[ 8] | NMI | AIN16 |      |  X00  |       | SCOM0.0| SCOM2.0 |TCC0/WO0|TCC1/WO2|I2S/SD1 |         |
 * |     9      | PA[ 9] |EXT09| AIN17 |      |  X01  |       | SCOM0.1| SCOM2.1 |TCC0/WO1|TCC1/WO3|I2S/MCK0|         |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    10      | PA[14] |EXT14|       |      |       |       | SCOM2.2| SCOM4.2 |TCC3/WO0|TCC0/WO4|        |GLCK_IO0 |
 * |    11      | PA[15] |EXT15|       |      |       |       | SCOM2.3| SCOM4.3 |TCC3/WO1|TCC0/WO5|        |GLCK_IO1 |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    12      | PA[18] |EXT02|       |      |  X06  |       | SCOM1.2| SCOM3.2 |TCC3/WO0|TCC0/WO2|        |AC/CMP0  |
 * |    13      | PA[19] |EXT03|       |      |  X07  |       | SCOM1.3| SCOM3.3 |TCC3/WO1|TCC0/WO3|        |AC/CMP1  |
 * |    14      | PA[20] |EXT04|       |      |  X08  |       | SCOM5.2| SCOM3.2 |TC7/WO0 |TCC0/WO6|I2S/SCK0|GCLK_IO4 |
 * |    15      | PA[21] |EXT05|       |      |  X09  |       | SCOM5.3| SCOM3.3 |TC7/WO1 |TCC0/WO7|I2S/FS0 |GCLK_IO5 |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|																													  |
 * |    16      | PA[27] |EXT15|       |      |       |       |        |         |        |TCC3/WO6|        |GCLK_IO0 |
 * |    17      | PA[28] |EXT08|       |      |       |       |        |         |        |TCC3/WO7|        |GCLK_IO0 |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|																													  |
 * |    18      | PA[30] |EXT10|       |      |       |       |        | SCOM1.2 |TCC1/WO0|TCC3/WO4|SWCLK   |GCLK_IO0 |
 * |    19      | PA[31] |EXT11|       |      |       |       |        | SCOM1.3 |TCC1/WO1|TCC3/WO5|SWDIO   |         |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|																													  
 * |    20      | PB[ 2] |EXT02| AIN10 |      |  Y08  |       |        | SCOM5.0 |TC6/WO0 |TCC3/WO2|        |         |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|		
 * |    21      | PB[ 8] |EXT08| AIN02 |      |  Y14  |       |        | SCOM4.0 |TC4/WO0 |TC3/WO6 |        |         |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    22      | PB[10] |EXT10|       |      |       |       |        | SCOM4.2 |TC5/WO0 |TCC0/WO4|I2S/MCK1|GCLK_IO4 |
 * |    23      | PB[11] |EXT11|       |      |       |       |        | SCOM4.3 |TC5/WO1 |TCC0/WO5|I2S/SCK1|GCLK_IO5 |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    24      | PB[22] |EXT06|       |      |       |       |        | SCOM5.2 |TC7/WO0 |TCC3/WO0|        |GCLK_IO0 |
 * |    25      | PB[23] |EXT07|       |      |       |       |        | SCOM5.3 |TC7/WO1 |TCC3/WO1|        |GCLK_IO1 |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |   SCOM1                                                                                                          |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    26      | PA[16] |EXT00|       |      |  X04  |       | SCOM1.0| SCOM3.0 |TCC2/WO0|TCC0/WO6|        |GLCK_IO2 |
 * |    27      | PA[17] |EXT01|       |      |  X05  |       | SCOM1.1| SCOM3.1 |TCC2/WO1|TCC0/WO7|        |GLCK_IO3 |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |   SCOM2                                                                                                          |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    28      | PA[12] |EXT12|       |      |       |       | SCOM2.0| SCOM4.0 |TCC2/WO0|TCC0/WO6|        |AC/CMP0  |
 * |    29      | PA[13] |EXT13|       |      |       |       | SCOM2.1| SCOM4.1 |TCC2/WO1|TCC0/WO7|        |AC/CMP1  |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |   SCOM3                                                                                                          |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    30      | PA[22] |EXT06|       |      |  X10  |       | SCOM3.0| SCOM5.0 |TC4/WO0 |TCC0/WO4|        |GCLK_IO6 |
 * |    31      | PA[23] |EXT07|       |      |  X11  |       | SCOM3.1| SCOM5.1 |TC4/WO1 |TCC0/WO5|USB/SOF |GCLK_IO7 |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |   USB                                                                                                            |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    32      | PA[24] |EXT12|       |      |       |       | SCOM3.2| SCOM5.2 |TC5/WO0 |TCC1/WO2|USB/DM  |         |
 * |    33      | PA[25] |EXT13|       |      |       |       | SCOM3.3| SCOM5.3 |TC5/WO1 |TCC1/WO3|USB/DP  |         |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |   Analog                                                                                                         |
 * |------------+--------+-----+-------+------+-------+-------+--------+---------+--------+--------+--------+---------|
 * |    34      | PA[10] |EXT10| AIN18 |      |  X02  |       | SCOM0.2| SCOM2.2 |TCC1/WO0|TCC0/WO2|I2S/SCK0|GCLK_IO4 |
 * |    35      | PA[11] |EXT11| AIN19 |      |  X03  |       | SCOM0.3| SCOM2.3 |TCC1/WO1|TCC0/WO3|I2S/FS0 |GCLK_IO5 |
 * |    36      | PB[ 3] |EXT03| AIN11 |      |  Y09  |       |        | SCOM5.1 |TC6/WO1 |TCC3/WO3|        |         |
 * |    37      | PB[ 9] |EXT09| AIN03 |      |  Y15  |       |        | SCOM4.1 |TC4/WO1 |TC3/WO7 |        |         |
 * +==================================================================================================================+
 */


#include "variant.h"
/*
 * Pins descriptions
 */
const PinDescription g_APinDescription[]=
{
  //Digital
  { PORTA,  0, PIO_DIGITAL, (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTA,  1, PIO_DIGITAL, (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTA,  2, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_ANALOG /*DAC*/        ), ADC_Channel0  , NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTA,  3, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  { PORTA,  4, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), ADC_Channel4  , PWM0_CH0  , TCC0_CH0    , EXTERNAL_INT_NONE },
  { PORTA,  5, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), ADC_Channel5  , PWM0_CH1  , TCC0_CH1    , EXTERNAL_INT_NONE },
  { PORTA,  6, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), ADC_Channel6  , NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTA,  7, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), ADC_Channel7  , NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTA,  8, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), ADC_Channel16 , NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NMI  }, 
  { PORTA,  9, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), ADC_Channel17 , NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  
   
  { PORTA, 14, PIO_DIGITAL, (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  { PORTA, 15, PIO_DIGITAL, (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  { PORTA, 18, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTA, 19, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), No_ADC_Channel, PWM3_CH1  , TC3_CH1     , EXTERNAL_INT_NONE }, 
  { PORTA, 20, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER_ALT), No_ADC_Channel, PWM0_CH6  , TCC0_CH6    , EXTERNAL_INT_4    },
  { PORTA, 21, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER_ALT), No_ADC_Channel, PWM0_CH7  , TCC0_CH7    , EXTERNAL_INT_5    },
  
  { PORTA, 27, PIO_DIGITAL, (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTA, 28, PIO_DIGITAL, (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  
  { PORTA, 30, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIO_SERCOM                     ), No_ADC_Channel, NOT_ON_PWM, TCC1_CH0    , EXTERNAL_INT_10   }, 
  { PORTA, 31, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIO_SERCOM                     ), No_ADC_Channel, NOT_ON_PWM, TCC1_CH1    , EXTERNAL_INT_11   }, 

  { PORTB,  2, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), ADC_Channel10 , NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_2    },
  
  { PORTB,  8, PIO_DIGITAL, (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  { PORTB, 10, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), No_ADC_Channel, PWM5_CH0  , TC5_CH0     , EXTERNAL_INT_10   },
  { PORTB, 11, PIO_DIGITAL, (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), No_ADC_Channel, PWM5_CH1  , TC5_CH1     , EXTERNAL_INT_11   },

  { PORTB, 22, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  { PORTB, 23, PIO_DIGITAL, (PIN_ATTR_DIGITAL                                ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  //SCOM1
  { PORTA, 16, PIO_SERCOM , (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), No_ADC_Channel, PWM2_CH0  , TCC2_CH0    , EXTERNAL_INT_0    }, 
  { PORTA, 17, PIO_SERCOM , (PIN_ATTR_DIGITAL                                ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_1    }, 
  //SCOM2
  { PORTA, 12, PIO_SERCOM , (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  { PORTA, 13, PIO_SERCOM , (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE },
  //SCOM3
  { PORTA, 22, PIO_SERCOM , (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), No_ADC_Channel, PWM4_CH0  , TC4_CH0     , EXTERNAL_INT_6    },
  { PORTA, 23, PIO_SERCOM , (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), No_ADC_Channel, PWM4_CH1  , TC4_CH1     , EXTERNAL_INT_7    },
  //USB
  { PORTA, 24, PIO_COM    , (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  { PORTA, 25, PIO_COM    , (PIN_ATTR_NONE                                   ), No_ADC_Channel, NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_NONE }, 
  //Analog
  { PORTA, 10, PIO_ANALOG , (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), ADC_Channel18 , PWM1_CH0  , TCC1_CH0    , EXTERNAL_INT_NONE },
  { PORTA, 11, PIO_ANALOG , (PIN_ATTR_DIGITAL|PIN_ATTR_PWM|PIN_ATTR_TIMER    ), ADC_Channel19 , PWM1_CH1  , TCC1_CH1    , EXTERNAL_INT_NONE },
  { PORTB,  3, PIO_ANALOG , (PIN_ATTR_DIGITAL                                ), ADC_Channel11 , NOT_ON_PWM, NOT_ON_TIMER, EXTERNAL_INT_3    },
  { PORTB,  9, PIO_ANALOG , (PIN_ATTR_PWM|PIN_ATTR_TIMER                     ), ADC_Channel3  , PWM4_CH1  , TC4_CH1     , EXTERNAL_INT_9    },
  
};

extern "C" {
    unsigned int PINCOUNT_fn() {
        return (sizeof(g_APinDescription) / sizeof(g_APinDescription[0]));
    }
}

const void* g_apTCInstances[TCC_INST_NUM+TC_INST_NUM]={ TCC0, TCC1, TCC2, TC3, TC4, TC5 } ;

// Multi-serial objects instantiation
SERCOM sercom1( SERCOM1 ) ;
SERCOM sercom2( SERCOM2 ) ;
SERCOM sercom3( SERCOM3 ) ;

Uart Serial( &sercom3, PIN_SERIAL_RX, PIN_SERIAL_TX , PAD_SERIAL_RX, PAD_SERIAL_TX);

void SERCOM3_Handler()
{
  Serial.IrqHandler();
}


