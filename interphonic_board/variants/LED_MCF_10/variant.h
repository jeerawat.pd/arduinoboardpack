/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  ***Modify for Support Interphonic Arduino Project (SAMD21)***
*/
#ifndef _VARIANT_MUSICAL_FOUNTAIN_BOARD_10_
#define _VARIANT_MUSICAL_FOUNTAIN_BOARD_10_
// The definitions here needs a SAMD core >=1.6.10
#define ARDUINO_SAMD_VARIANT_COMPLIANCE 10610

/*----------------------------------------------------------------------------
 *        Definitions
 *----------------------------------------------------------------------------*/

/** Frequency of the board main oscillator */
#define VARIANT_MAINOSC		(32768ul)

/** Master clock frequency */
#define VARIANT_MCK			  (48000000ul)

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "WVariant.h"

#ifdef __cplusplus
#include "SERCOM.h"
#include "Uart.h"
#endif // __cplusplus

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*----------------------------------------------------------------------------
 *        Pins
 *----------------------------------------------------------------------------*/

// Number of pins defined in PinDescription array
#ifdef __cplusplus
extern "C" unsigned int PINCOUNT_fn();
#endif
#define PINS_COUNT           (PINCOUNT_fn())
#define NUM_DIGITAL_PINS     (34u)
#define NUM_ANALOG_INPUTS    (4u)
#define NUM_ANALOG_OUTPUTS   (0u)

#define digitalPinToPort(P)        ( &(PORT->Group[g_APinDescription[P].ulPort]) )
#define digitalPinToBitMask(P)     ( 1 << g_APinDescription[P].ulPin )
#define portOutputRegister(port)   ( &(port->OUT.reg) )
#define portInputRegister(port)    ( &(port->IN.reg) )
#define portModeRegister(port)     ( &(port->DIR.reg) )
#define digitalPinHasPWM(P)        ( g_APinDescription[P].ulPWMChannel != NOT_ON_PWM || g_APinDescription[P].ulTCChannel != NOT_ON_TIMER )

/*
 * digitalPinToTimer(..) is AVR-specific and is not defined for SAMD
 * architecture. If you need to check if a pin supports PWM you must
 * use digitalPinHasPWM(..).
 *
 * https://github.com/arduino/Arduino/issues/1833
 */
/*
 * pins
 */
#define PA0  (0u)
#define PA1  (1u)
#define PA2  (2u)
#define PA3  (3u)
#define PA4  (4u)
#define PA5  (5u)
#define PA6  (6u)
#define PA7  (7u)
#define PA8  (8u)
#define PA9  (9u)
/*-----------------*/
#define PA14 (10u)
#define PA15 (11u)
/*-----------------*/
#define PA18 (12u)
#define PA19 (13u)
#define PA20 (14u)
#define PA21 (15u)
/*-----------------*/
#define PA27 (16u)
#define PA28 (17u)
/*-----------------*/
#define PA30 (18u)
#define PA31 (19u)
/*-----------------*/
#define PB2  (20u)
/*-----------------*/
#define PB8  (21u)
/*-----------------*/
#define PB10 (22u)
#define PB11 (23u)
/*-----------------*/
#define PB22 (24u)
#define PB23 (25u)
/*-----------------*/
#define PA16 (26u)
#define PA17 (27u)
/*-----------------*/
#define PA12 (28u)
#define PA13 (29u)
/*-----------------*/
#define PA22 (30u)
#define PA23 (31u)
/*-----------------*/
#define PA24 (32u)
#define PA25 (33u)
/*-----------------*/
#define PA10 (34u)
#define PA11 (35u)
#define PB3  (36u)
#define PB9  (37u)
/*-----------------*/


/*
 * Analog pins
 */
#define PIN_A0               PA10
#define PIN_A1               PA11
#define PIN_A2               PB3
#define PIN_A3               PB9

static const uint32_t A0  = PIN_A0,
					  A1  = PIN_A1,
					  A2  = PIN_A2,
					  A3  = PIN_A3;

static const uint32_t A[4]  = {PIN_A0,PIN_A1,PIN_A2,PIN_A3};

#define ADC_RESOLUTION		12

/*
 * Digital pins
 */
static const uint32_t PA[32]  = {PA0 ,PA1 ,PA2 ,PA3 ,PA4 ,PA5 ,PA6 ,PA7 ,PA8 ,PA9 ,PA10,PA11,PA12,PA13,PA14,PA15,
								 PA16,PA17,PA18,PA19,PA20,PA21,PA22,PA23,PA24,PA25,5000,PA27,PA28,5000,PA30,PA31 };
static const uint32_t PB[32]  = {5000,5000,PB2 ,PB3 ,5000,5000,5000,5000,PB8 ,PB9 ,PB10,PB11,5000,5000,5000,5000,
								 5000,5000,5000,5000,5000,5000,PB22,PB23,5000,5000,5000,5000,5000,5000,5000,5000 };
								 
								 

//Internal Specific pin
#define PIN_RW485           PA21

/*
 * Serial interfaces
 */
// Serial
#define PIN_SERIAL_TX       PA22
#define PIN_SERIAL_RX       PA23
#define PAD_SERIAL_TX       (UART_TX_PAD_0)
#define PAD_SERIAL_RX       (SERCOM_RX_PAD_1)

/*
 * SPI Interfaces
 */
#define SPI_INTERFACES_COUNT 1

#define PIN_SPI_MISO         PA18
#define PIN_SPI_MOSI         PA16
#define PIN_SPI_SCK          PA17
#define PERIPH_SPI           sercom1
#define PAD_SPI_TX           SPI_PAD_0_SCK_1
#define PAD_SPI_RX           SERCOM_RX_PAD_3

static const uint8_t MOSI = PIN_SPI_MOSI ;
static const uint8_t MISO = PIN_SPI_MISO ;
static const uint8_t SCK  = PIN_SPI_SCK  ;

/*
 * Wire Interfaces
 */
#define WIRE_INTERFACES_COUNT 1

#define PIN_WIRE_SDA         PA12
#define PIN_WIRE_SCL         PA13
#define PERIPH_WIRE          sercom2
#define WIRE_IT_HANDLER      SERCOM2_Handler

static const uint8_t SDA = PIN_WIRE_SDA;
static const uint8_t SCL = PIN_WIRE_SCL;

/*
 * USB
 */   
#define PIN_USB_DM          PA24
#define PIN_USB_DP          PA25
 
#ifdef __cplusplus
}
#endif

/*----------------------------------------------------------------------------
 *        Arduino objects - C++ only
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus

/*	=========================
 *	===== SERCOM DEFINITION
 *	=========================
*/
extern SERCOM sercom1;
extern SERCOM sercom2;
extern SERCOM sercom3;

extern Uart Serial;

#endif

// These serial port names are intended to allow libraries and architecture-neutral
// sketches to automatically default to the correct port name for a particular type
// of use.  For example, a GPS module would normally connect to SERIAL_PORT_HARDWARE_OPEN,
// the first hardware serial port whose RX/TX pins are not dedicated to another use.
//
// SERIAL_PORT_MONITOR        Port which normally prints to the Arduino Serial Monitor
//
// SERIAL_PORT_USBVIRTUAL     Port which is USB virtual serial
//
// SERIAL_PORT_LINUXBRIDGE    Port which connects to a Linux system via Bridge library
//
// SERIAL_PORT_HARDWARE       Hardware serial port, physical RX & TX pins.
//
// SERIAL_PORT_HARDWARE_OPEN  Hardware serial ports which are open for use.  Their RX & TX
//                            pins are NOT connected to anything by default.
#define SERIAL_PORT_USBVIRTUAL      SerialUSB
#endif