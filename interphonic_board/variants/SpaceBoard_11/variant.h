/*
  Copyright (c) 2014-2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

  ***Modify for Support Interphonic Arduino Project (SAMD21)***
*/
#ifndef _VARIANT_SPACE_BOARD_11_
#define _VARIANT_SPACE_BOARD_11_
// The definitions here needs a SAMD core >=1.6.10
#define ARDUINO_SAMD_VARIANT_COMPLIANCE 10610

/*----------------------------------------------------------------------------
 *        Definitions
 *----------------------------------------------------------------------------*/

/** Frequency of the board main oscillator */
#define VARIANT_MAINOSC		(32768ul)

/** Master clock frequency */
#define VARIANT_MCK			  (48000000ul)

/*----------------------------------------------------------------------------
 *        Headers
 *----------------------------------------------------------------------------*/

#include "WVariant.h"

#ifdef __cplusplus
#include "SERCOM.h"
#include "Uart.h"
#endif // __cplusplus

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

/*----------------------------------------------------------------------------
 *        Pins
 *----------------------------------------------------------------------------*/

// Number of pins defined in PinDescription array
#ifdef __cplusplus
extern "C" unsigned int PINCOUNT_fn();
#endif
#define PINS_COUNT           (PINCOUNT_fn())
#define NUM_DIGITAL_PINS     (13u)
#define NUM_ANALOG_INPUTS    (10u)
#define NUM_ANALOG_OUTPUTS   (0u)
#define analogInputToDigitalPin(p)  ((p < 10u) ? (p) : -1)

#define digitalPinToPort(P)        ( &(PORT->Group[g_APinDescription[P].ulPort]) )
#define digitalPinToBitMask(P)     ( 1 << g_APinDescription[P].ulPin )
//#define analogInPinToBit(P)        ( )
#define portOutputRegister(port)   ( &(port->OUT.reg) )
#define portInputRegister(port)    ( &(port->IN.reg) )
#define portModeRegister(port)     ( &(port->DIR.reg) )
#define digitalPinHasPWM(P)        ( g_APinDescription[P].ulPWMChannel != NOT_ON_PWM || g_APinDescription[P].ulTCChannel != NOT_ON_TIMER )

/*
 * digitalPinToTimer(..) is AVR-specific and is not defined for SAMD
 * architecture. If you need to check if a pin supports PWM you must
 * use digitalPinHasPWM(..).
 *
 * https://github.com/arduino/Arduino/issues/1833
 */
/*
 * Analog pins
 */
#define PIN_A0               (0u)
#define PIN_A1               (1u)
#define PIN_A2               (2u)
#define PIN_A3               (3u)
#define PIN_A4               (4u)
#define PIN_A5               (5u)
#define PIN_A6               (6u)
#define PIN_A7               (7u)
#define PIN_A8               (8u)
#define PIN_A9               (9u)

static const uint32_t A0  = PIN_A0;
static const uint32_t A1  = PIN_A1;
static const uint32_t A2  = PIN_A2;
static const uint32_t A3  = PIN_A3;
static const uint32_t A4  = PIN_A4;
static const uint32_t A5  = PIN_A5;
static const uint32_t A6  = PIN_A6;
static const uint32_t A7  = PIN_A7;
static const uint32_t A8  = PIN_A8;
static const uint32_t A9  = PIN_A9;

#define ADC_RESOLUTION		12

/*
 * Digital pins
 */
#define PIN_D0               (10u)
#define PIN_D1               (11u)
#define PIN_D2               (12u)
#define PIN_D3               (13u)
#define PIN_D4               (14u)
#define PIN_D5               (15u)
#define PIN_D6               (16u)
#define PIN_D7               (17u)
#define PIN_D8               (18u)
#define PIN_D9               (19u)
#define PIN_D10              (20u)
#define PIN_D11              (21u)
#define PIN_D12              (22u)

//MCP23017 - Addr 00
#define PIN_Dx0			 	 (0x30u)
#define PIN_Dx1			 	 (0x31u)
#define PIN_Dx2			 	 (0x32u)
#define PIN_Dx3			 	 (0x33u)
#define PIN_Dx4			 	 (0x34u)
#define PIN_Dx5			 	 (0x35u)
#define PIN_Dx6			 	 (0x36u)
#define PIN_Dx7			 	 (0x37u)  

//Expander Specific pin
#define PIN_ESP_RST     	(0x38u)
#define PIN_ETH_RST         (0x39u)
#define PIN_GSM_RST         (0x3Au)
#define PIN_GSM_DAT_DIS     (0x3Bu)
#define PIN_LED0            (0x3Cu)
#define PIN_LED1            (0x3Du)
#define PIN_LED2            (0x3Eu)
#define PIN_LCD_RST         (0x3Fu)

//MCP23017 - Addr 01
 #define PIN_LCD_LED0		(0x40u)
 #define PIN_LCD_LED1		(0x41u)
 #define PIN_LCD_LED2		(0x42u)
 #define PIN_LCD_UP			(0x43u)
 #define PIN_LCD_DW			(0x44u)
 #define PIN_LCD_LT			(0x45u)
 #define PIN_LCD_RT			(0x46u)
 #define PIN_LCD_OK			(0x47u)
 #define PIN_LCD_RS			(0x48u)
 #define PIN_LCD_RW			(0x49u)
 #define PIN_LCD_EN			(0x4Au)
 #define PIN_LCD_BL			(0x4Bu)
 #define PIN_LCD_D4			(0x4Cu)
 #define PIN_LCD_D5			(0x4Du)
 #define PIN_LCD_D6			(0x4Eu)
 #define PIN_LCD_D7			(0x4Fu)
 
static const uint32_t D0   = PIN_D0;
static const uint32_t D1   = PIN_D1;
static const uint32_t D2   = PIN_D2;
static const uint32_t D3   = PIN_D3;
static const uint32_t D4   = PIN_D4;
static const uint32_t D5   = PIN_D5;
static const uint32_t D6   = PIN_D6;
static const uint32_t D7   = PIN_D7;
static const uint32_t D8   = PIN_D8;
static const uint32_t D9   = PIN_D9;
static const uint32_t D10  = PIN_D10;
static const uint32_t D11  = PIN_D11;
static const uint32_t D12  = PIN_D12;

static const uint32_t Dx0  = PIN_Dx0;
static const uint32_t Dx1  = PIN_Dx1;
static const uint32_t Dx2  = PIN_Dx2;
static const uint32_t Dx3  = PIN_Dx3;
static const uint32_t Dx4  = PIN_Dx4;
static const uint32_t Dx5  = PIN_Dx5;
static const uint32_t Dx6  = PIN_Dx6;
static const uint32_t Dx7  = PIN_Dx7;

//Internal Specific pin
#define PIN_LORA_SEL    	PIN_D0
#define PIN_ETH_CS          PIN_D1
#define PIN_LORA_RST        PIN_D2
#define PIN_GSM_WAKE        PIN_D3
#define PIN_SD_CS           PIN_D4
#define PIN_RW485           PIN_D5
#define PIN_LORA_DIO0       PIN_D6
#define PIN_USB_HOST_ENABLE PIN_D7
#define PIN_SWCLK           PIN_D8
#define PIN_SWDIO           PIN_D9
#define PIN_LCD_INT         PIN_D10
#define PIN_EPORT_INT       PIN_D11
#define PIN_ETH_INT         PIN_D12


/*
 * Serial interfaces
 */
// Serial0 (GSM)
#define PIN_SERIAL0_RX       (24u)
#define PIN_SERIAL0_TX       (23u)
#define PAD_SERIAL0_TX       (UART_TX_PAD_0)
#define PAD_SERIAL0_RX       (SERCOM_RX_PAD_1)

// Serial4 (WIFI)
#define PIN_SERIAL4_RX      (26u)
#define PIN_SERIAL4_TX      (25u)
#define PAD_SERIAL4_TX      (UART_TX_PAD_2)
#define PAD_SERIAL4_RX      (SERCOM_RX_PAD_3)

// Serial5 (RS485)
#define PIN_SERIAL5_RX      (28u)
#define PIN_SERIAL5_TX      (27u)
#define PAD_SERIAL5_TX      (UART_TX_PAD_0)
#define PAD_SERIAL5_RX      (SERCOM_RX_PAD_1)


/*
 * SPI Interfaces
 */
#define SPI_INTERFACES_COUNT 1

#define PIN_SPI_SS			 PIN_ETH_CS
#define PIN_SPI_MISO         (31u)
#define PIN_SPI_MOSI         (29u)
#define PIN_SPI_SCK          (30u)
#define PERIPH_SPI           sercom1
#define PAD_SPI_TX           SPI_PAD_0_SCK_1
#define PAD_SPI_RX           SERCOM_RX_PAD_3

static const uint8_t MOSI = PIN_SPI_MOSI ;
static const uint8_t MISO = PIN_SPI_MISO ;
static const uint8_t SCK  = PIN_SPI_SCK  ;

/*
 * Wire Interfaces
 */
#define WIRE_INTERFACES_COUNT 1

#define PIN_WIRE_SDA         (32u)
#define PIN_WIRE_SCL         (33u)
#define PERIPH_WIRE          sercom2
#define WIRE_IT_HANDLER      SERCOM2_Handler

static const uint8_t SDA = PIN_WIRE_SDA;
static const uint8_t SCL = PIN_WIRE_SCL;

/*
 * USB
 */   
#define PIN_USB_DM          (34u)
#define PIN_USB_DP          (35u)
 
#ifdef __cplusplus
}
#endif

/*----------------------------------------------------------------------------
 *        Arduino objects - C++ only
 *----------------------------------------------------------------------------*/

#ifdef __cplusplus

/*	=========================
 *	===== SERCOM DEFINITION
 *	=========================
*/
extern SERCOM sercom0;
extern SERCOM sercom1;
extern SERCOM sercom2;
extern SERCOM sercom3;
extern SERCOM sercom4;
extern SERCOM sercom5;

extern Uart Serial0;
extern Uart Serial4;
extern Uart Serial5;

#endif

// These serial port names are intended to allow libraries and architecture-neutral
// sketches to automatically default to the correct port name for a particular type
// of use.  For example, a GPS module would normally connect to SERIAL_PORT_HARDWARE_OPEN,
// the first hardware serial port whose RX/TX pins are not dedicated to another use.
//
// SERIAL_PORT_MONITOR        Port which normally prints to the Arduino Serial Monitor
//
// SERIAL_PORT_USBVIRTUAL     Port which is USB virtual serial
//
// SERIAL_PORT_LINUXBRIDGE    Port which connects to a Linux system via Bridge library
//
// SERIAL_PORT_HARDWARE       Hardware serial port, physical RX & TX pins.
//
// SERIAL_PORT_HARDWARE_OPEN  Hardware serial ports which are open for use.  Their RX & TX
//                            pins are NOT connected to anything by default.
#define SERIAL_PORT_USBVIRTUAL      SerialUSB
#define Serial                      SerialUSB
#define GSMcomm                     Serial0
#define WIFIcomm                    Serial4
#define RS485comm                   Serial5
#endif /* _VARIANT_SPACE_BOARD_11_ */